#!/bin/sh
echo "Installing phalcon"
sudo apt-add-repository ppa:phalcon/stable;
sudo apt-get update;
sudo apt-get install php5-phalcon -y;
echo "Installing dependencies through composer";
cd /var/www/rubber_phalcon;
sudo php composer.phar install;
sudo php composer.phar update;
echo "Initialize and migrate database";
cd /var/www/rubber_phalcon ;
sudo vendor/bin/phinx migrate;
echo "Installing php5-dev"
sudo apt-get install php5-dev
echo "Installing Gearman server";
sudo apt-get install gearman-job-server libgearman-dev -y;
echo "Installing Gearman tools"
sudo apt-get install gearman-tools;
echo "Installing Gearman PHP extension";
pecl install gearman;
echo "Adding gearman extension to php.ini";
echo 'extension=gearman.so' >> /etc/php5/fpm/php.ini;
echo 'extension=gearman.so' >> /etc/php5/cli/php.ini;
echo "Installing screen"
sudo apt-get install screen;
echo "Restarting service";
sudo service php5-fpm restart;